const tabsBtnWork = document.querySelectorAll(".work-tabs-nav-btn");
const tabsItemsWork = document.querySelectorAll(".work-tabs-item");

tabsBtnWork.forEach(onTabClick);

function onTabClick(item) {
    item.addEventListener('click', function () {
        let currentBtnWork = item;
        let tabIdWork = currentBtnWork.getAttribute('data-tab');
        let currentTabWork = document.querySelector(tabIdWork);

        if (!currentBtnWork.classList.contains('active')) {
            tabsBtnWork.forEach(function (item) {
                item.classList.remove('active');
            });
    
            tabsItemsWork.forEach(function (item) {
                item.classList.remove('active');
            });
    
            currentBtnWork.classList.add('active');
            currentTabWork.classList.add('active');
        }
    });
};

document.querySelector('.work-tabs-nav-btn').click();